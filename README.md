COTA - SDK

El SDK de COTA se compone de dos librerías, una “Data Analytics” y la otra “OTA Promotion”. Estas librerías brindan la capacidad de recopilar información sobre el dispositivo e impulsar diferentes tipos de campañas de notificaciones e instalación de apps de forma silenciosa. 


Requerimientos mínimos para la implementación de COTA SDK :

Requerimientos mínimos de App Contenedor:
SDK versión mínima: 24
SDK version target: 30
Soporte para AndroidX
Requerimientos mínimos de las terminales:
Android 7.x o superior
1 GB de RAM


     Permisos requeridos en manifest.xml

<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.READ_PHONE_STATE" />
<uses-permission android:name="android.permission.INTERNET" />
 

Proceso de instalación

Siga los siguientes pasos para su  integración:


En el archivo "build.gradle" en el nivel del proyecto, agregue los siguientes repositorios en la sección "allprojects - repositories": 


maven { url 'https://api.bitbucket.org/2.0/repositories/epictechdevteam/public-release/src/master' }
maven { url 'https://jitpack.io' }

Internamente se utiliza el SDK "ksoap2-android". Esto está alojado en un repositorio diferente de los agregados por defectos, por lo tanto, el repositorio debe agregarse en la sección "allprojects - repositories" del archivo "build.gradle" a nivel de proyecto:

maven { url 'https://oss.sonatype.org/content/repositories/ksoap2-android-releases/' }


En el archivo "build.gradle" a nivel de módulo, agregue la siguiente dependencia:

implementation 'io.imox.tools:dataanalyticsota:1.0.13'


El SDK utiliza un conjunto de librerías de AndroidX, por lo que su aplicación debe ser compatible con AndroidX, para ello, agregue las siguientes líneas a su archivo gradle.properties:



android.useAndroidX=true
android.enableJetifier=true

En el archivo "AndroidManifest.xml" de su aplicación, debe agregar el permiso de uso de Internet y la meta-data con el ID_BRAND proporcionado por IMOX:


<uses-permission android:name="android.permission.INTERNET"/>

<application>
    <meta-data
         android:name="ID_BRAND"
         android:value="[ID_BRAND]" />
</application>








En el método "onCreate" de la clase principal del proyecto que se extiende desde la clase “Application”, la librería debe inicializarse agregando las siguientes líneas de código:


public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            ImoxTools.initialize(this, TIME_RECOLEC_DATA, TIME_FETCH_OTA);
        } catch (Exception e) {
            // TODO handle the exception
        }
    }
}

Reemplace TIME_RECOLEC_DATA con un valor largo que representa el intervalo de tiempo en milisegundos en el que desea que la herramienta recopile y envíe la información periódicamente.

Reemplace TIME_FETCH_OTA con un valor largo que representa el intervalo de tiempo en milisegundos  en el que desea que la herramienta busque campañas publicitarias activas.




Construido con :

Java : Lenguaje de programación
Postgres SQL : Base de datos
WIN : Plataforma Web

Versionado :

Para este proyecto se usa Bitbucket como manejador de versiones de código.

Autor :

IMOX Technologies S.A

 Programadores:  

     Elmer Palacios (Team Leader Android at IMOX)
     Katherine Rivas (Android Developer at IMOX)



